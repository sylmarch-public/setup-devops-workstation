ANSIBLE_DEPENDENCIES_DIR = ./dependencies
ANSIBLE_BIN = ansible-playbook
BECOME_METHOD ?= su
BECOME_FLAGS ?= -
ANSIBLE_OPTS = --diff --connection=local -i "127.0.0.1," -K --become-method "$(BECOME_METHOD)" -e ansible_become_flags="$(BECOME_FLAGS)"

.PHONY: all
all: clean install

.PHONY: install
install: install_ansible install_requirements

.PHONY: install_ansible
install_ansible:
	@echo "Installation Ansible"
	pipx install --include-deps ansible

.PHONY: install_requirements
install_requirements:
	@echo "Installation des collections et roles Ansible"
	ansible-galaxy install -r requirements.yml

.PHONY: check_local
check_local:
	@echo "Déploiement de la configuration (dry-run)"
	$(ANSIBLE_BIN) --check $(ANSIBLE_OPTS) playbook-devops-workstation.yml

.PHONY: deploy_local
deploy_local:
	@echo "Déploiement de la configuration"
	$(ANSIBLE_BIN) $(ANSIBLE_OPTS) playbook-devops-workstation.yml

.PHONY: check_work
check_work:
	@echo "Exécution playbook de travail (dry-run)"
	$(ANSIBLE_BIN) --check $(ANSIBLE_OPTS) playbook-work.yml

.PHONY: deploy_work
deploy_work:
	@echo "Exécution playbook de travail"
	$(ANSIBLE_BIN) $(ANSIBLE_OPTS) playbook-work.yml

.PHONY: clean
clean:
	@echo "Suppression $(ANSIBLE_DEPENDENCIES_DIR)"
