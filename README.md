# setup-devops-workstation

Playbook Ansible pour installer des outils et configurer une station de travail DevOps.

Cette installation est "opinionated", notamment de part l'arborescence pour organiser le workspace Visual Studio Code, les plugins installés et leurs configurations.

Le playbook déploie notamment :

- Des paquets de base
- Ansible
- Git
- Docker CE
- Visual Studio Code avec un ensemble de plugins (Ansible, Markdown, Python, ...)
- Vagrant
- Des aliases et configurations pour un shell bash
- Des settings pour Gnome
- Le déploiement et l'activation du pare-feu UFW en bloquant toutes connexions entrantes

Pour savoir comment utiliser ce projet, rendez-vous directement à la [section correspondante](#comment-utiliser-ce-playbook-).

[[_TOC_]]

## Licence

Tout ce contenu est en [licence MIT](./LICENSE.txt). Vous pouvez [consulter ce lien](https://choosealicense.com/licenses/mit/) si vous ne savez pas ce que cela signifie.

Si vous trouvez une erreur, faites m'en part pour que je puisse corriger ce repository pour que votre retour puisse bénéficier au reste de la communauté.

## OS supportés

Ce playbook a été testé sur les OS suivants :

- Ubuntu 24.04 (noble) avec Gnome

Les versions précédentes de ce playbook ont été testées sous les OS suivants mais il n'est plus garanti qu'il fonctionne toujours sur ceux-ci :

- Debian 12 (bookworm) avec Gnome
- Debian 11 (bullseye) avec Gnome
- Ubuntu 22.04 (jammy) avec Gnome

## Description

### Rôles Ansible

Le playbook est décomposé en plusieurs rôles Ansible présents dans le répertoire `<projet>/roles/`.

N'hésitez pas à consulter un rôle pour visualiser en détail ses actions. Si vous n'êtes pas familier d'Ansible :

- le point d'entrée d'un rôle correspond au fichier `<projet>/roles/<nom_role>/tasks/main.yml`
- les variables par défaut sont définies dans le fichier  `<projet>/roles/<nom_role>/default/main.yml`
- les variables peuvent être surchargées dans le fichier `<projet>/group_vars/vars.yml`

### Paquets de base

Les paquets / logiciels de base suivants sont installés :

#### Clients HTTP

| Paquet       | Description |
| ------------ | ----------- |
| `bruno`      | Client graphique pour tester des API (REST, GraphQL, SOAP)<br/>Alternative libre à Postman.<br/>[Site web](https://docs.usebruno.com) |
| `curl`       | Client HTTP "couteau suisse" |
| `httpie`     | Client HTTP moins connu mais facile d'utilisation. Fourni la commande `http`. [Voir quelques exemples](https://httpie.io/docs/cli/examples). |
| `wget`       | Un autre client HTTP "couteau suisse" |

#### Base de données

| Paquet       | Description |
| ------------ | ----------- |
| `sqlitebrowser`       | Logiciel DB Browser for SQLite.<br/>GUI pour visualiser et interagir avec des bases de données SQLite. |

#### Gestionnaire de paquets

| Paquet       | Description |
| ------------ | ----------- |
| `npm`        | Gestionnaire de paquets lié à NodeJS. |

#### Monitoring

| Paquet       | Description |
| ------------ | ----------- |
| `glances`    | Monitoring complet de diverses métriques (CPU, RAM, Disk I/O, Filesystem, ...).<br/>[Voir quelques screenshots](https://nicolargo.github.io/glances/). |
| `htop`       | Monitoring CPU/RAM/PID... par processus. |
| `iotop`      | Monitoring des I/O. |

#### Multimedia

| Paquet       | Description |
| ------------ | ----------- |
| `gimp`       | Editeur pour manipuler et retoucher des images. |
| `inkscape`   | Editeur pour les images vectorielles (SVG entre autres). |
| `vlc`        | Lecteur audio/vidéo. |

#### Utilitaires

| Paquet       | Description |
| ------------ | ----------- |
| `colordiff` | Wrapper autour de la commande `diff` pour ajouter de la coloration. |
| `jq` | Manipulation, formatage et recherche dans des documents JSON ([cheatsheet](https://lzone.de/cheat-sheet/jq)). |
| `less` | Remplace la commande `more` ("less is more").<br/>Permet de naviguer facilement dans un fichier texte ou un résultat de commande. |
| `python3-pip` | Installe le gestionnaire `pip` pour pouvoir installer des paquets Python 3 via ce gestionnaire. |
| `remmina` | Application pour se connecter sur des machines distantes en RDP, VNC, etc... |
| `terminator` | Terminal qui peut être splitté facilement. Possibilité d'exécuter des commandes simultanément dans plusieurs terminaux. |
| `tree` | Affichage d'une arborescence de fichiers/répertoires. |
| `vim` | Editeur de texte dans un terminal. |
| `yq` | Equivalent de `jq` pour les fichiers YAML. |

### Visual Studio Code - Organisation des workspaces/projets/configurations

J'organise les workspaces/projets/configurations de Visual Studio Code de la manière suivante.

Tous les éléments sont regroupés dans l'arborescence `${HOME}/vscode-workspace/`.

Chaque sous-répertoire correspond à un serveur distant servant de référence. Par exemple, les projets récupérés depuis `github.com` seront présents dans l'arborescence `${HOME}/vscode-workspace/github.com/`.

Les projets, créés ou clonés, sont positionnés en fonction du serveur distant servant de référence. L'arborescence sur le serveur distant est reproduite localement pour s'y retrouver plus facilement.

Par exemple, le repository <https://github.com/torvalds/linux> sera positionné dans le répertoire `${HOME}/vscode-workspace/github.com/torvalds/linux`.

Visual Studio Code permet de regrouper les projets dans des workspaces. Je vous conseille d'enregistrer tous les fichiers `nom.code-workspace` directement à la racine du répertoire `${HOME}/vscode-workspace/`. Pour enregistrer un workspace : menu `File > Save workspace as...`.

Par exemple, vous pouvez choisir de regrouper :

- les projets en lien avec votre client A dans un workspace `client-A.code-workspace`)
- vos projets personnels dans un workspace `perso.code-workspace`
- les projets en lien avec un périmètre dans un workspace `perimetre-X.code-workspace`

Au vu de ces considérations, vous devez obtenir une arborescence ressemblant à ceci :

```bash
${HOME}/vscode-workspace
# 1 FICHIER *.code-workspace PAR WORKSPACE
├── client-A.code-workspace
├── client-B.code-workspace
├── perso.code-workspace
├── formation.code-workspace
│
# 1 REPERTOIRE PAR SERVEUR DISTANT SERVANT DE REFERENCE
├── github.com
│   ├── docker
│   │   ├── buildx
│   │   ├── cli
│   │   └── compose
│   └── torvalds
│       └── linux  # CORRESPOND A https://github.com/torvalds/linux/
├── gitlab.com
│   └── sylmarch
│       └── setup-devops-workstation
│   └── xavki
│       ├── presentation-ansible-fr
│       ├── presentations-cassandra
│       └── presentations-terraform-fr
└── gitlab.mycompany.com
    ├── client-A
    │   ├── projetA1
    │   ├── projetA2
    │   └── projetA3
    └── client-B
        ├── projetA1
        └── projetA2
```

### Git - Configuration par défaut et configuration par repository distant

De par l'organisation des workspace(s) Visual Studio Code décrite ci-dessus, il est facile de mettre en oeuvre des configurations Git différentes en fonction du serveur distant.

Par exemple, il est possible d'utiliser votre compte professionnel pour un usage professionnel et un compte personnel pour les projets personnels.

La distinction s'effectue grâce à l'emplacement du projet dans l'arborescence `${HOME}/vscode-workspace/<git-repo>/path/to/project`

Une configuration par défaut est également définie.

La configuration Git appliquée inclut :

- le nom de l'utilisateur et son adresse e-mail
- la gestion de l'identifiant pour se connecter au repository Git :
    - login de l'utilisateur (connexion HTTP)
    - ou emplacement de la clé privée (connexion SSH)
- la gestion d'un cache pour ne pas avoir à ressaisir le mot de passe

Des alias `cd<id-repo>` sont créés automatiquement pour se positionner directement dans le répertoire qui correspond à ce repository Git. Par exemple, pour `github.com`, l'alias `cdgithub.com` permet de se positionner dans le répertoire `${HOME}/vscode-workspace/github.com`.

L'alias `cdgit` permet de se positionner directement dans le repository Git défini avec la clé `cdgit_alias: true`. Il est donc conseillé de positionner cette clé sur le repository Git que vous utilisez le plus.

| Description                                           | Emplacement           |
| :---------------------------------------------------- | :-------------------- |
| Configuration Git                                     | Fichier `${HOME}/.gitconfig`  |
| Configuration client SSH (notamment choix clé privée) | Fichier `${HOME}/.ssh/config` |

Pour en savoir plus, consultez le rôle [git](./roles/git/).

### Visual Studio Code - Raccourcis claviers

Voici quelques raccourcis clavier que j'utilise au quotidiennement et qui vous seront rapidement utiles.

Pensez au nombre de fois où vous alternez entre votre clavier et votre souris

&rArr; **Moins vous utiliserez votre souris, plus vous gagnerez en vélocité et en confort.**

#### Raccourcis personnalisés ajoutés

Des raccourcis claviers sont définis dans le profil de l'utilisateur en écrasant le fichier `${HOME}/.config/Code/User/keybindings.json`

| Raccourci clavier | Description |
| ----------------- | ----------- |
| `Alt + A`         | Toggle entre le fichier ouvert dans la zone d'édition et le terminal. |
| `Tab`             | Dans un fichier Markdown, passage à la cellule suivante lorsque le curseur est dans une cellule d'un tableau sans reformater ce tableau. (voir le plugin `Markdown Table`) |
| `Shift + Tab`             | Dans un fichier Markdown, passage à la cellule précédente lorsque le curseur est dans une cellule d'un tableau sans reformater ce tableau. (voir le plugin `Markdown Table`) |
| `Ctrl + F4`             | Ferme l'onglet actif. |

Certains raccourcis sont supprimés :

| Raccourci clavier | Description |
| ----------------- | ----------- |
| `Ctrl + Q`         | Fermeture du workspace. |
| `Ctrl + W`         | Fermeture de l'éditeur. |

#### Raccourcis hyper-utiles pré-existants dans Visual Studio Code

| Raccourci clavier  | Description |
| ------------------ | ----------- |
| `Ctrl + Espace`    | Auto-complétion. Sert aussi à découvrir des fonctionnalités, méthodes, ... |
| `Ctrl + P`         | Recherche un fichier dans les projets ouverts. |
| `Ctrl + Shift + P` | Ouvrir la palette pour les commandes dans Visual Studio Code (ex : `Git: Create Branch...`). |
| `Ctrl + Tab` / `Ctrl + Shift + Tab` | Aller à l'onglet suivant/précédent. |
| `Ctrl + S`         | Enregistre le fichier courant. |
| `Ctrl + F`         | Rechercher dans le fichier courant. |
| `Ctrl + H`         | Remplacer dans le fichier courant. |
| `Ctrl + Shift + F` | Rechercher dans les fichiers de tous les projets dans le workspace. |
| `Ctrl + G`         | Aller à la ligne n°XXX. |
| `Alt + Up` / `Alt + Down` | Déplacer la ligne courante ou les lignes sélectionnées vers le haut/bas. |

#### Raccourcis hyper-utiles existants dans de nombreux logiciels

Et d'autres raccourcis clavier qui fonctionnent dans de nombreux logiciels :

| Raccourci clavier       | Description |
| ----------------------- | ----------- |
| `Home`                  | Déplacer le curseur en début de ligne. |
| `Fin`                   | Déplacer le curseur en fin de ligne. |
| `Ctrl + Gauche` / `Ctrl + Droite` | Déplacer le curseur de mot en mot. |
| `Ctrl + Home`           | Déplacer le curseur en haut du fichier. |
| `Ctrl + Fin`            | Déplacer le curseur en bas du fichier. |
| `Ctrl + Retour arrière` | Supprimer le contenu du curseur jusqu'au début du mot courant. |
| `Ctrl + Suppr`          | Supprimer le contenu du curseur jusqu'à la fin du mot courant. |
| _Combinaison ci-dessus_ `+ Shift` | Déplacer le curseur en sélectionnant le texte depuis la position initiale du curseur. |

### Visual Studio Code - Settings utilisateurs

Les préférences utilisateurs sont définis dans le profil de l'utilisateur en écrasant le fichier `${HOME}/.config/Code/User/settings.json`.

Quelques settings majeurs :

- l'utilisation de `Save Actions`, notamment pour reformater automatiquement vos modifications (ex : suppression automatique de trailing spaces)
- non-affichage du bouton `Commit` dans la vue `Source Control`. Un bouton bleu n'apparaît que si vous avez des interactions "en attente" avec le repository de référence (soit merger des modifications du serveur distant vers votre branche locale, soit pousser vos modifications vers le repository distant). Un bouton `Commit` plus petit est toujours disponible dans cette vue `Source Control`.
- configuration d'actions pour `git` (ex : fetch automatique toute les X minutes, prune automatique des branches supprimées sur le remote, ...)
- ajustement des règles pour le linter Markdown
- affichage des caractères whitespaces
- affichage de rulers à 80, 100 et 120 caractères
- désactivation de la télémétrie

### Visual Studio Code - Extensions et configurations

#### Plugins et configurations de base

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_base_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker) | Correcteur orthographique basique pour certaines typos grossières.<br/><br/>Note : ne corrige pas les erreurs grammaticales (ex : accords, pluriels, ...) contrairement à [languagetool](https://languagetool.org) mais qui est payant dans un cadre professionnel ([voir le blog de Stéphane Robert](https://blog.stephane-robert.info/post/langagetool-visual-studio-code/) si vous voulez l'utiliser) |
| [Code Spell Checker - Français](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-french) | Support du français pour Code Spell Checker. |
| [Commitlint](https://marketplace.visualstudio.com/items?itemName=joshbolduc.commitlint) | Vérification du format des messages de commits avec `commitlint`.<br/>`commitlint` doit être configuré projet par projet.<br/>Voir l'[annexe ci-dessous](#commitlint---comment-initialiser-un-projet). |
| [Dev Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) | Environnement de développement au sein de conteneurs Docker. |
| [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph) | Visualisation graphique de l'historique des commits/branches d'un projet Git. |
| [Git Lens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) | Support étendu de Git dans Visual Studio Code. |
| [Indent rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow) | Amélioration coloration pour l'indentation. Met rapidement en évidence des problèmes d'indentation. |
| [Project Manager](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager) | Facilite la bascule entre différents workspaces de Visual Studio Code. |
| [RedHat YAML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) | Support pour les fichiers YAML |
| [Remote - SSH](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh) | Permet de travailler sur une machine distante accessible en SSH dans l'environnement Visual Studio Code. Tous les plugins, extensions, scripts... sont exécutés/installés sur la machine distante pour ne pas polluer votre machine locale. |
| [SVN](https://marketplace.visualstudio.com/items?itemName=johnstoncode.svn-scmsystem) | Support de SVN. |
| [WorkspaceSort](https://marketplace.visualstudio.com/items?itemName=iciclesoft.workspacesort) | Tri des projets dans le workspace par ordre alphabétique. |

#### Ansible

`ansible` et `ansible-lint` sont installés d'office via `pip` grâce au rôle `ansible`.

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_ansible_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [Ansible](https://marketplace.visualstudio.com/items?itemName=redhat.ansible) | Support d'Ansible. |
| [Better Jinja](https://marketplace.visualstudio.com/items?itemName=samuelcolvin.jinjahtml) | Support pour d'avantage de fichiers Jinja. |

#### Apache HTTPD

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_apache_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [Apache Conf Syntax Highlighter](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-apache) | Coloration syntaxique pour Apache HTTPD. |
| [Apache Conf Snippets](https://marketplace.visualstudio.com/items?itemName=eiminsasete.apacheconf-snippets) | Snippets pour des configurations Apache HTTPD. Le nom des snippets commencent par `a-` (ex : `a-exclude-url-from-redirection`). |

#### Bash

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_bash_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [shell-format](https://marketplace.visualstudio.com/items?itemName=foxundermoon.shell-format) | Formatter pour bash et d'autres formats. |
| [Bash Debug](https://marketplace.visualstudio.com/items?itemName=rogalmic.bash-debug) | Debug de script bash. |
| [ShellCheck for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=timonwong.shellcheck) | Linter bash. Corrections d'erreurs courantes et apport de conseils. |
| [shellman](https://marketplace.visualstudio.com/items?itemName=Remisa.shellman) | Snippet pour script bash.<br/>- `shebang` : création du shebang.<br/>- `parse args` : parsing des options passés au script<br/>- `region` : ajout de commentaires pour isoler clairement des sections du scripts<br/>- `array forEach` : itération sur les éléments d'un tableau<br/>- ... |

#### Docker

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_docker_extensions: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) | Support de Docker. |
| [shell-format](https://marketplace.visualstudio.com/items?itemName=foxundermoon.shell-format) | Formateur pour les fichiers `Dockerfile`. |
| [hadolint](https://marketplace.visualstudio.com/items?itemName=exiasr.hadolint) | Linter pour les fichiers `Dockerfile`. |
| [Docker dive](https://marketplace.visualstudio.com/items?itemName=sandipchitale.docker-dive) | Invocation de l'utilitaire `dive` pour analyser les layers d'une image Docker. |

Des scripts wrappers sont également déployées :

- `hadolint` : wrappe le lancement de `hadolint` dans un conteneur Docker.
- `dive` : wrappe le lancement de `dive` dans un conteneur Docker.

#### GitLab

`gitlabci-local` et `glab` sont installés d'office via `pip` grâce au rôle `gitlab`.

`glab` est la CLI de GitLab. Elle permet notamment de clone tous les projets dans un groupe via une seule commande. ([voir la documentation officielle et ses fonctionnalités](https://gitlab.com/gitlab-org/cli)).

`gitlabci-local` permet d'exécuter des jobs de la GitLab CI/CD localement sur votre poste. Ceci est pratique notamment pour les phases de mise au point d'un job ou du fichier `.gitlab-ci.yml` ([voir le projet sous PyPI](<https://pypi.org/project/gitlabci-local/>)).

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_gitlab_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [GitLab VS Code Extension](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow) | Interaction avec GitLab depuis Visual Studio Code.<br/>⚠️ Ce plugin nécessite de réaliser une configuration manuelle pour y déplacer votre token.as  |

#### Java

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_java_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |

NON IMPLEMENTE POUR LE MOMENT

#### Markdown

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_markdown_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [Markdown Preview Mermaid Support](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid) | Visualisation du rendu des sections `mermaid` dans un fichier markdown. |
| [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) | Support notamment des raccourcis clavier comme :<br/>- `Ctrl + B` - Mise en gras<br/>- `Ctrl + I` - Mise en italique |
| [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint) | Linter pour les fichiers Markdown. |
| [Markdown Table](https://marketplace.visualstudio.com/items?itemName=TakumiI.markdowntable) | Fonctionnalités supplémentaires pour les tableaux en markdown.<br/>Ex :<br/>- Navigation de cellule en cellule via la touche `Tab` et `Shift + Tab`.<br/>- Insertion d'une colonne ou d'une ligne<br/>- ...<br/><br/>Ce plugin est configuré pour ne pas appliquer un formatage automatique sur les tableaux car ce formatage est problématique lorsque les cellules contiennent énormalement de texte. |

#### PHP

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_php_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |

NON IMPLEMENTE POUR LE MOMENT

#### Python

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_python_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [Microsoft Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python) | Support de Python. |
| [Python Indent](https://marketplace.visualstudio.com/items?itemName=KevinRose.vsc-python-indent) | Corrige certaines indentations. |
| [Python Test Explorer](https://marketplace.visualstudio.com/items?itemName=littlefoxteam.vscode-python-test-adapter) | Facilite la manipulation des tests unitaires dans Visual Studio Code. |
| [Python Type Hint](https://marketplace.visualstudio.com/items?itemName=njqdev.vscode-python-typehint) | Aide à l'ajout et à la vérification des typehint sur les fonctions. |
| [autoDocstring - Python Docstring Generator](https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring) | Facilite la génération des Docstring pour documenter chaque fonctions. |
| [Microsoft flake8](https://marketplace.visualstudio.com/items?itemName=ms-python.flake8) | Linter `flake8`. |
| [Microsoft isort](https://marketplace.visualstudio.com/items?itemName=ms-python.isort) | Tri des dépendances (import), notamment par ordre alphabétique. |

Les packages suivants seront également installés via `pip` :

- `autopep8`
- `isort`

#### TypeScript

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_typescript_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |

NON IMPLEMENTE POUR LE MOMENT

#### Vagrant

Les éléments ci-dessous s'appliquent uniquement si `vscode_config_vagrant_enabled: true`.

| Plugin Visual Studio Code | Description |
| ------------- | ----------- |
| [Vagrant](https://marketplace.visualstudio.com/items?itemName=bbenoist.vagrant) | Support de Vagrant dans Visual Studio Code. |

## Pare-feu UFW

Le playbook installe et active le pare-feu UFW.

Par défaut, le pare-feu bloque toutes les connexions entrantes et routées et autorise toutes les requêtes sortantes (comportement classique pour un PC et non pas un serveur).

Ainsi, vous évitez que des tiers accèdent à des services que vous exposeriez par mégarde sur une interface physique de votre PC.

Par exemple, si vous démarrez un conteneur Docker ainsi, vous bindez le service Nginx sur le port `8080` de TOUTES les interfaces de l'hôte :

```bash
$ docker run -ti -p 8080:80 nginx

# dans un autre terminal, on constate que le port 8080 est bindé sur tous les interfaces réseau de l'hôte (cf adresse d'écoute) :
$ ss -lnpt | grep :8080
LISTEN 0      4096         0.0.0.0:8080       0.0.0.0:*
LISTEN 0      4096            [::]:8080          [::]:*
```

Pour ne pas exposer le service à l'extérieur de l'hôte, il faut binder sur sa boucle locale :

```bash
$ docker run -ti -p 127.0.0.1:8080:80 nginx

# dans un autre terminal, on constate que le port 8080 est bindé uniquement sur la boucle locale de l'hôte et donc le service n'est pas accessible depuis un autre PC :
$ ss -lnpt | grep :8080
LISTEN 0      4096       127.0.0.1:8080       0.0.0.0:*
```

### Visualiser les règles de filtrage

Pour visualiser les règles de filtrage :

```bash
$ ufw status verbose
État : actif
Journalisation : on (low)
Par défaut : deny (incoming), allow (outgoing), deny (routed)
Nouveaux profils : skip
```

### Autoriser des connexions entrantes

Pour autoriser les connexions SSH entrantes :

- depuis n'importe quelle machine et les tracer dans les journaux : `ufw allow in log 22/tcp comment "Accès SSH"`
- uniquement depuis une IP spécifique et les tracer dans les journaux : `ufw allow in log proto tcp from 10.11.22.33 to any port 22 comment "Accès SSH depuis IP 10.11.22.33"`

Pour autoriser les connexions HTTPS entrantes depuis une IP spécifique vers le port 443 et les tracer dans les journaux : `ufw allow in log proto tcp from 10.11.22.33 to any port 443 comment "Accès temporaire HTTPS (TCP/443)"`

### Supprimer des autorisations

Le plus simple est de lister les règles et de procéder à une suppression via leur index.

Pour lister les règles par index : `ufw status numbered`

Pour supprimer la règle via son index : `ufw delete indexno`

:warning: Si vous supprimez plusieurs règles, faites attention lors de la suppression de la seconde car son index peut ne plus être le même !

## Personnalisation du terminal avec powerline.bash

Le terminal en base bash est personnalisé en utilisant le [script powerline.bash](https://gitlab.com/bersace/powerline.bash) et des variables définies dans le fichier `~/.bashrc`.

Il permet notamment d'avoir rapidement :

- l'horodatage de fin de la commande précédente
- l'exit status de la commande précédente
- visualiser la branche git courante et son état
- visualiser l'environnement Python actuellement activé
- et plein d'autres éléments (voir le [dépôt d'Etienne Bersac](https://gitlab.com/bersace/powerline.bash) et son [cookbook](https://gitlab.com/bersace/powerline.bash/-/blob/master/docs/COOKBOOK.md)).

Il utilise également des polices de [Nerd fonts](https://github.com/ryanoasis/nerd-fonts/) (et indirectement Powerline), en particulier la police `UbuntuMono Nerd Font Mono`.

Un aperçu du rendu :

![Aperçu rendu terminal avec powerline.bash](assets/terminal-avec-powerline.bash.png)

ATTENTION : ne pas oublier dans les différents terminaux de sélectionner la police `UbuntuMono Nerd Font Mono` sinon les icônes ne s'afficheront pas correctement.

- Dans VScode, voir ces 2 paramètres :

    ```json
    "terminal.integrated.fontFamily": "\"UbuntuMono Nerd Font Mono\"",
    "terminal.integrated.fontSize": 16,
    ```

- Dans Terminator :
    - `Clic droit > Préférences` > `Onglet Profil` > `Sous onglet Général`
    - Décocher `Utiliser la police à chasse fixe du système`
        - Mettre comme police de caractères : `UbuntuMono Nerd Font Mono` avec une taille de `14`.

- Dans Gnome Terminal (i.e. terminal par défaut sous Gnome)
    - `Menu Préférences`
    - Cliquer sur le profil courant `Sans nom`
    - Onglet `Texte`
        - Cocher ̀`Police personnalisée`
        - Mettre comme police de caractères : `UbuntuMono Nerd Font Mono` avec une taille de `14`.

## Comment utiliser ce playbook ?

### Pré-requis

1. Installez l'utilitaire `make` :

    ```bash
    sudo apt install make
    ```

2. Installez l'utilitaire `pipx` :

    ```bash
    sudo apt install pipx
    ```

3. Ajouter le répertoire des binaires installés par `pipx` dans votre PATH :

    ```bash
    pipx ensurepath
    ```

4. Fermer le terminal puis le réouvrir pour prendre en compte la mise à jour de la variable `PATH`

Note : il est possible d'exécuter le playbook à distance sur un autre hôte via SSH sans qu'Ansible soit installé sur la machine distante.

### Préparation

1. Récupérez l'archive du projet [dans les releases](https://gitlab.com/sylmarch-public/setup-devops-workstation/-/releases) et décompressez-la.
2. Installez Ansible sur la machine cible ainsi que ces dépendances (collections & rôles) :

    ```bash
    make install
    ```

3. Renommez le fichier `<projet>/group_vars/all/vars.yml` en `<projet>/group_vars/all/vars.yml.example`

4. Editez le fichier `<projet>/group_vars/all/vars.yml` selon votre besoin, notamment pour :
    - définir vos repositories Git et les configurations associées.
    - les catégories de plugins/configurations que vous voulez appliquer à Visual Studio Code.

    Exemple pour un utilisateur `John DOE` qui dispose de :

    - 2 comptes Git personnels, l'un sous sous `github.com` et l'autre sur `gitlab.com`, utilisant une connexion SSH (cf propriété `ssh_private_key`). Chaque compte dispose de sa propre clé SSH.
    - 1 compte Git professionnel sous `gitlab.mycompany.com` utilisant une connexion HTTPS (cf propriété `http_user`)

    ```yml
    ---
    # GLOBAL CONFIGURATION
    proxy_env:
    enabled: false
    http_proxy: http://192.168.0.2:3128
    https_proxy: http://192.168.0.2:3128
    no_proxy:
        - localhost
        - 127.0.0.1

    # GIT CONFIGURATION
    git_default_name: John DOE (default)
    git_default_email: john.doe.personal-email@example.com
    git_repos:
    github.com:
        url: https://github.com
        name: John DOE for GitHub
        email: john.doe.personal-email@example.com
        ssh_private_key: ~/.ssh/id_ed25519-github.com
        cache_timeout: 14400
    gitlab.com:
        url: https://gitlab.com
        name: John DOE for GitLab
        email: john.doe.personal-email@example.com
        ssh_private_key: ~/.ssh/id_ed25519-gitlab.com
        cache_timeout: 14400
    gitlab.mycompany.com:
        url: https://gitlab.mycompany.com
        name: John DOE for MyCompany
        http_user: johndoe
        email: john.doe@mycompany.com
        cache_timeout: 14400
        cdgit_alias: true

    # VSCODE GENERAL CONFIGURATION
    vscode_config_workspace_root: "{{ ansible_env.HOME }}/vscode-workspace"
    vscode_config_base_enabled: true

    # VSCODE SPECIFIC/LANGUAGE/TECHNO CONFIGURATION
    vscode_config_ansible_enabled: true
    vscode_config_apache_enabled: true
    vscode_config_bash_enabled: true
    vscode_config_docker_enabled: true
    vscode_config_gitlab_enabled: true
    vscode_config_java_enabled: false
    vscode_config_markdown_enabled: true
    vscode_config_php_enabled: false
    vscode_config_python_enabled: true
    vscode_config_typescript_enabled: false
    vscode_config_vagrant_enabled: true
    ```

### Exécution du playbook - Application des changements

Exécutez le playbook en indiquant la méthode pour réaliser l'élévation de privilèges.

Sous Debian - élévation de privilèges via `su` (testé sous Debian 12) :

```bash
export BECOME_METHOD="su"
export BECOME_FLAGS="-"
make deploy_local
```

Sous Ubuntu - élévation de privilèges via `sudo` (testé sous Ubuntu 24.04) :

```bash
export BECOME_METHOD="sudo"
export BECOME_FLAGS="-i"
make deploy_local
```

Note : Si vous souhaitez uniquement visualiser les modifications qui seront apportées sans que celles-ci ne soient appliquées, exécutez la cible `check_local` plutôt que `deploy_local`.

### Post-installation

Certaines actions ne peuvent pas être réalisées facilement de manière automatique. Vous devez alors réaliser les actions suivantes.

#### Terminal - Sélection de la police de caractères

Ajuster la police de caractères dans les différentes terminaux :

- VSCode (normalement déjà géré via le fichier `settings.json`)
- Terminator
- Gnome Terminal

Voir la [section dédiée](#personnalisation-du-terminal-avec-powerlinebash) qui présente la procédure.

#### Visual Studio Code - Extension GitLab Workflow

Uniquement si `vscode_config_gitlab_enabled: true`.

Dans l'interface web de GitLab, créer un personal access token :

1. Cliquez sur votre icône puis `Edit profile`
2. Cliquez sur `Access token`
3. Dans la section `Active personal access tokens`, cliquez sur `Add new token`
   1. Renseignez le nom du token, par exemple en utilisant le nom de votre machine : `nom_machine - GitLab VS Code Extension`
   2. Sélectionnez le scope `api`
   3. Mettez une date d'expiration dans 1 an (valeur max)
   4. Cliquez sur `Create personal access token`
4. Copiez le token

Dans Visual Studio Code :

1. Cliquez sur l'icône `GitLab Workflow` ou faites `Ctrl + Shift + P` puis sélectionnez `GitLab: Authenticate`
2. Renseignez l'URL de votre serveur GitLab (ex : `https://gitlab.com`)*
3. Sélectionner le mode `Personal Access Token`
4. Sélectionner le mode `Enter existing access token`
5. Collez votre personal access token

Exemple d'utilisation du plugin sans avoir besoin de vous réauthentifiez sur le serveur GitLab :

- Ouverture du pipeline qui vient de se lancer suite au push d'un commit vers le repository
    - `Ctrl + Shift + P` puis sélectionnez `GitLab: Pipeline Actions - View, Create, Retry, or Cancel`
- Ouverture du fichier actif sur le repository GitLab
    - `Ctrl + Shift + P` puis sélectionnez `GitLab: Open Active File on GitLab`
- Création d'une MergeRequest à partir de la branche courante
    - `Ctrl + Shift + P` puis sélectionnez `GitLab: Create New Merge Request on Current Project`
- etc...

Note : Lorsque votre token expirera, il faudra commencer par le supprimer dans VS Code pour pouvoir ensuite enregistrer un nouveau (`Ctrl + Shift + P` puis sélectionnez `GitLab: Remove Account to VS Code`).

### Vagrant - Configuration des plugins

Plusieurs plugins pour Vagrant sont installés. Vous voudrez sans doute configurés certains plugins pour faciliter leur utilisation au quotidien. Consultez les documentations de chaque plugin pour en savoir plus.

| Plugin Vagrant | Description |
| -------------- | ----------- |
| [vagrant-hostmanager](https://github.com/devopsgroup-io/vagrant-hostmanager) | Mise à jour automatique du fichier `/etc/hosts` de l'hôte et/ou des VMs. |
| [vagrant-proxyconf](https://github.com/tmatilai/vagrant-proxyconf) | Configuration automatique du proxy pour différente briques (APT, Docker, ...) dans les VMs. |

## Annexes

### GIT - Comment générer un couple de clés SSH

```bash
# Génère un couple de clés asymétiques pour gitlab.com
# - Clé privée :   ~/.ssh/id_ed25519-gitlab.com
# - Clé publique : ~/.ssh/id_ed25519-gitlab.com.pub
GIT_REPO_HOSTNAME=gitlab.com
ssh-keygen -t ed25519 -f "${HOME}/.ssh/id_ed25519-${GIT_REPO_HOSTNAME}"
```

Puis dans la déclaration du repository (fichier `<projet>/group_vars/all/vars.yml`), définir la clé `ssh_private_key` :

```yml
git_repos:
  # ...
  gitlab.com:
    url: https://gitlab.com:
    name: John DOE
    email: john.doe@example.com
    ssh_private_key: ~/.ssh/id_ed25519-gitlab.com
    cache_timeout: 14400
```

### Commitlint - Comment initialiser un projet

Commitlint ne fonctionne que lorsqu'il est configuré dans le projet courant.

Pour configurer commitlint dans le projet courant en utilisant les [conventional commits](https://www.conventionalcommits.org/fr/v1.0.0/), [suivre cette documentation](https://commitlint.js.org/guides/getting-started.html)

1. Installer les dépendances (nécessite `npm`) :

    ```bash
    npm install --save-dev @commitlint/{cli,config-conventional}
    ```

2. Créer le fichier `.commitlintrc.yml` :

    ```yml
    ---
    extends:
    - "@commitlint/config-conventional"

    ````

## Annexe - Développement

### Environnement de développement - Exécution du playbook

```bash
USER=john
IP=192.168.56.2

BECOME_METHOD=su
BECOME_FLAGS="-"

# Visualiser les changements qui seront appliqués = aucune modification apportée (option --check)
ansible-playbook --diff --check -i "${IP}," -u "${USER}" -k -K --become-method "${BECOME_METHOD}" -e ansible_become_flags="${BECOME_FLAGS}" playbook-devops-workstation.yml

# Réaliser les modifications
ansible-playbook --diff -i "${IP}," -u "${USER}" -k -K --become-method "${BECOME_METHOD}" -e ansible_become_flags="${BECOME_FLAGS}" playbook-devops-workstation.yml
```

### Afficher les facts Ansible

```bash
ansible all -i "${IP}," -u "${USER}" -k -m ansible.builtin.setup
```
